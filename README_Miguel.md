Hi there!
Here you have the requested API's endpoints implemented.

I refactored adding simple MVC layers to create a better architecture as well.
Sadly, I ran out of time, so I didn't implemented all I wanted to show.

Even so, I would like to say the things I wanted to add to the proof:

- Add an express router to acommodate all the endpoints/routes
- Add Jest and make at least some unitary tests for happy paths and test.each for negative cases.
- Test services, mocking DB layer responses.
- Create some integration test using supertest & Jest
- Typescript
- A better approach of error manager

Notes:

- I included a postman library I created to test the calls
- SQLite3 version is deprecated and incompatible with my current Node.js version (18.15.0) so I updated it on package.json
- model "default" clause is not working. I replace it with "defaultValue".
