const express = require("express");
const bodyParser = require("body-parser");
const { sequelize } = require("./model");

const { getProfile, isClient, dateChecker } = require("./middleware");

const {
  contractController,
  jobController,
  balanceController,
  adminController,
} = require("./controllers");

const app = express();
app.use(bodyParser.json());
app.set("sequelize", sequelize);
app.set("models", sequelize.models);

/**
 * Returns a list of contracts belonging to a user (client or contractor),
 * the list should only contain non terminated contracts.
 */
app.get(
  "/contracts",
  getProfile,
  contractController.getNonTerminatedContractsByProfileId
);

/**
 * return the contract only if it belongs to the profile calling
 */
app.get("/contracts/:id", getProfile, contractController.getContractById);

/**
 * Get all unpaid jobs for a user (**_either_** a client or contractor),
 * for **_active contracts only_**.
 */
app.get("/jobs/unpaid", getProfile, jobController.getUnpaidJobsByProfileId);

/**
 * Pay for a job, a client can only pay if his balance >= the amount to pay.
 * The amount should be moved from the client's balance to the contractor balance.
 */
app.post(
  "/jobs/:job_id/pay",
  getProfile,
  isClient,
  jobController.payJobByClientToContractor
);

/**
 * Deposits money into the balance of a client,
 * a client can't deposit more than 25% his total of jobs to pay. (at the deposit moment)
 */
app.post(
  "/balances/deposit/:profileId",
  getProfile,
  isClient,
  balanceController.depositBalanceByProfileId
);

/**
 * Returns the profession that earned the most money (sum of jobs paid)
 * for any contactor that worked in the query time range.
 */
app.get(
  "/admin/best-profession",
  dateChecker,
  adminController.getBestProfession
);

/**
 * Returns the clients the paid the most for jobs in the query time period.
 * limit query parameter should be applied, default limit is 2.
 */
app.get("/admin/best-clients", dateChecker, adminController.getBestClients);

module.exports = app;
