const { sequelize, Op } = require("../model");
const { Contract, Profile, Job } = sequelize.models;

const getNonTerminatedContractsByProfileId = async (profileId) => {
  return await Contract.findAll({
    where: {
      [Op.not]: { status: "terminated" },
      [Op.or]: [{ clientId: profileId }, { ContractorId: profileId }],
    },
  });
};

const getContractById = async (contractId) => {
  return await Contract.findOne({ where: { id: contractId } });
};

const getUnpaidJobsByProfileId = async (profileId) => {
  return await Job.findAll({
    where: {
      paid: false,
    },
    include: {
      model: Contract,
      where: {
        [Op.or]: [{ clientId: profileId }, { ContractorId: profileId }],
        [Op.not]: { status: "terminated" },
      },
    },
  });
};

const getJobById = async (jobId) => {
  return await Job.findOne({
    where: {
      id: jobId,
    },
    include: Contract,
  });
};

const getProfileById = async (profileId) => {
  return await Profile.findOne({
    where: { id: profileId },
  });
};

const payJobByClientToContractor = async (job, client, contractor) => {
  return await sequelize.transaction(async (t) => {
    await Job.update(
      { paid: true, paymentDate: Date.now() },
      { where: { id: job.id } },
      { transaction: t }
    );

    await Profile.update(
      { balance: client.balance - job.price },
      { where: { id: client.id } },
      { transaction: t }
    );

    await Profile.update(
      { balance: contractor.balance + job.price },
      { where: { id: contractor.id } },
      { transaction: t }
    );
  });
};

const totalCostOfUnpaidJobsByClientId = async (clientId) => {
  const costFromUnpaidJobs = await Job.findAll({
    attributes: [
      [sequelize.fn("sum", sequelize.col("Job.price")), "totalAmount"],
    ],
    raw: true,
    where: {
      paid: false,
    },
    include: {
      model: Contract,
      attributes: [],
      where: {
        clientId,
      },
    },
  });

  return costFromUnpaidJobs[0].totalAmount ?? 0;
};

const increaseProfileBalance = async (profileId, depositAmount) => {
  const client = await getProfileById(profileId);
  client.balance += depositAmount;
  return await client.save();
};

const getBestProfession = async (startDate, endDate) => {
  return await Job.findAll({
    attributes: [
      "ContractId",
      [sequelize.fn("sum", sequelize.col("Job.price")), "priceByContractId"],
    ],
    raw: true,
    where: {
      paid: true,
      paymentDate: { [Op.between]: [startDate, endDate] },
    },
    group: "ContractId",
    order: [["priceByContractId", "DESC"]],
    limit: 1,
  });
};

const getBestClient = async (startDate, endDate, limit) => {
  return await Job.findAll({
    attributes: [
      ["price", "paid"],
      [sequelize.col("Contract.ClientId"), "id"],
      [sequelize.literal("firstName || ' ' || lastName"), "full_name"],
    ],
    raw: true,
    where: {
      paid: true,
      paymentDate: { [Op.between]: [startDate, endDate] },
    },
    order: [["paid", "DESC"]],
    limit,
    include: [
      {
        model: Contract,
        attributes: [],
        include: [
          {
            model: Profile,
            as: "Client",
            attributes: [],
          },
        ],
      },
    ],
  });
};

module.exports = {
  getNonTerminatedContractsByProfileId,
  getContractById,
  getUnpaidJobsByProfileId,
  getJobById,
  getProfileById,
  payJobByClientToContractor,
  totalCostOfUnpaidJobsByClientId,
  increaseProfileBalance,
  getBestProfession,
  getBestClient,
};
