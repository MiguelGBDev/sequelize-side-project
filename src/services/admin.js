const db = require("../db");

const getBestProfession = async (startDate, endDate) => {
  return await db.getBestProfession(startDate, endDate);
};

const getBestClients = async (startDate, endDate, limit = 2) => {
  return await db.getBestClient(startDate, endDate, limit);
};

module.exports = { getBestProfession, getBestClients };
