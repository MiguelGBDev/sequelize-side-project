const db = require("../db");

const getNonTerminatedContractsByProfileId = async (profileId) => {
  const contracts = await db.getNonTerminatedContractsByProfileId(profileId);

  return contracts;
};

const getContractById = async (contractId, profileId) => {
  const contract = await db.getContractById(contractId);

  if (!contract) {
    throw new Error("Not found");
  }

  const { ClientId, ContractorId } = contract;
  if (![ClientId, ContractorId].includes(profileId)) {
    throw new Error("Forbidden");
  }

  return contract;
};

module.exports = { getNonTerminatedContractsByProfileId, getContractById };
