const db = require("../db");

const getUnpaidJobsByProfileId = async (profileId) => {
  const unpaidJobs = await db.getUnpaidJobsByProfileId(profileId);

  return unpaidJobs;
};

const payJobByClientToContractor = async (jobId, client) => {
  const job = await db.getJobById(jobId);

  if (!job) throw new Error("Not found");

  if (job.paid) throw new Error("Job paid already");

  if (job.price > client.balance) throw new Error("Not enough balance");

  if (job.Contract?.ClientId !== client.id) throw new Error("Forbidden");

  const contractor = await db.getProfileById(job.Contract?.ContractorId);

  await db.payJobByClientToContractor(job, client, contractor);

  return {
    clientId: client.id,
    jobId: job.id,
    contractorId: contractor.id,
    paid: true,
  };
};

module.exports = { getUnpaidJobsByProfileId, payJobByClientToContractor };
