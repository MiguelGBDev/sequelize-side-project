const db = require("../db");

const depositBalanceByProfileId = async (clientId, depositAmount) => {
  const totalCostFromJobs = await db.totalCostOfUnpaidJobsByClientId(clientId);

  const depositThreshold = totalCostFromJobs / 4;

  if (depositAmount > depositThreshold) {
    throw new Error(`Amount superior than allowed (${depositThreshold})`);
  }

  const response = await db.increaseProfileBalance(clientId, depositAmount);
  return response;
};

module.exports = { depositBalanceByProfileId };
