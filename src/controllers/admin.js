const { adminService } = require("../services");

const getBestProfession = async (req, res) => {
  const { startDate, endDate } = req;

  try {
    const bestProfession = await adminService.getBestProfession(
      startDate,
      endDate
    );
    res.json(bestProfession);
  } catch (e) {
    res.status(500).end("Service unavailable");
  }
};

const getBestClients = async (req, res) => {
  const { startDate, endDate } = req;
  const { limit } = req.query;

  try {
    const bestClients = await adminService.getBestClients(
      startDate,
      endDate,
      limit
    );

    res.json(bestClients);
  } catch (error) {
    res.status(500).end("Service unavailable");
  }
};

module.exports = { getBestProfession, getBestClients };
