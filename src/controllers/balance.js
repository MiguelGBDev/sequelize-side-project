const { balanceService } = require("../services");

const depositBalanceByProfileId = async (req, res) => {
  const {
    profile: { id: clientId },
  } = req;
  const { amount } = req.body;

  try {
    const response = await balanceService.depositBalanceByProfileId(
      clientId,
      amount
    );
    res.json(response);
  } catch (e) {
    if (e.message.includes("Amount superior than allowed"))
      return res.status(403).end(e.message);
    else res.status(500).end("Service unavailable");
  }
};

module.exports = { depositBalanceByProfileId };
