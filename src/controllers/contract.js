const { contractService } = require("../services");

const getNonTerminatedContractsByProfileId = async (req, res) => {
  const {
    profile: { id: profileId },
  } = req;

  try {
    const contracts =
      await contractService.getNonTerminatedContractsByProfileId(profileId);
    res.status(200).json(contracts);
  } catch (error) {
    res.status(500).json("Service unavailable");
  }
};

const getContractById = async (req, res) => {
  const { id: contractId } = req.params;
  const {
    profile: { id: profileId },
  } = req;

  try {
    const contract = await contractService.getContractById(
      contractId,
      profileId
    );

    res.json(contract);
  } catch (e) {
    if (e.message === "Not found") res.status(404).end(e.message);
    else if (e.message === "Forbidden") res.status(403).end(e.message);
    else res.status(500).json("Service unavailable");
  }
};

module.exports = { getNonTerminatedContractsByProfileId, getContractById };
