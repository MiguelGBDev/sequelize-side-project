const { jobService } = require("../services");

const getUnpaidJobsByProfileId = async (req, res) => {
  const {
    profile: { id: profileId },
  } = req;

  try {
    const unpaidJobs = await jobService.getUnpaidJobsByProfileId(profileId);
    res.json(unpaidJobs);
  } catch (error) {
    res.status(500).json("Service unavailable");
  }
};

const payJobByClientToContractor = async (req, res) => {
  const { profile: client } = req;
  const { job_id: jobId } = req.params;

  try {
    const response = await jobService.payJobByClientToContractor(jobId, client);
    res.json(response);
  } catch (e) {
    if (e.message === "Not found") return res.status(404).end(e.message);
    else if (e.message === "Job paid already")
      return res.status(403).end(e.message);
    else if (e.message === "Not enough balance")
      return res.status(403).end(e.message);
    else if (e.messsage === "Forbidden") return res.status(403).end(e.message);
    else res.status(500).end("Service unavailable");
  }
};

module.exports = { getUnpaidJobsByProfileId, payJobByClientToContractor };
