const dateChecker = async (req, res, next) => {
  const { start, end } = req.query;
  const startDate = new Date(start);
  const endDate = new Date(end);

  if (!isValidDate(startDate) || !isValidDate(endDate))
    return res.status(401).end(`incorrect period times`);

  req.startDate = startDate;
  req.endDate = endDate;
  next();
};
module.exports = { dateChecker };

const isValidDate = (date) => {
  if (Object.prototype.toString.call(date) === "[object Date]") {
    if (isNaN(date)) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
};
