const isClient = async (req, res, next) => {
  const { profile: client } = req;

  if (client.type !== "client") {
    return res.status(401).json("This operation is only allowed for clients");
  }

  next();
};
module.exports = { isClient };
